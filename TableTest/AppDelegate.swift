//
//  AppDelegate.swift
//  TableTest
//
//  Created by Peter Gosling on 4/19/19.
//  Copyright © 2019 Peter Gosling. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

  @IBOutlet weak var window: NSWindow!
  @IBOutlet weak var tableView: NSTableView!
  

  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // Insert code here to initialize your application
    self.tableView.register(NSNib.init(nibNamed: "TestCell", bundle: Bundle.main), forIdentifier: NSUserInterfaceItemIdentifier.testCell)
    self.tableView.delegate = self
    self.tableView.dataSource = self
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }
}

extension AppDelegate: NSTableViewDelegate, NSTableViewDataSource {
  
  func numberOfRows(in tableView: NSTableView) -> Int {
    return 50
  }
  
  func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
    if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier.testCell, owner: nil) as? TestCell {
      cell.label.stringValue = String(row)
      return cell
    }
    return nil
  }
  
  func tableViewSelectionDidChange(_ notification: Notification) {
    if let tableViewObject = notification.object as? NSTableView {
      let index = tableViewObject.selectedRow
      if index >= 0 {
        self.tableView.hideRows(at: IndexSet(integersIn: 1...10), withAnimation: .slideUp)
        self.tableView.hideRows(at: IndexSet(integersIn: 1...10), withAnimation: .slideUp)
      }
    }
  }
  
  func tableView(_ tableView: NSTableView, didRemove rowView: NSTableRowView, forRow row: Int) {
    print(row)
  }
}
