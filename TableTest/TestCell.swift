//
//  TestCell.swift
//  TableTest
//
//  Created by Peter Gosling on 4/19/19.
//  Copyright © 2019 Peter Gosling. All rights reserved.
//

import Cocoa

class TestCell: NSTableCellView {

  @IBOutlet weak var label: NSTextField!
  override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}


extension NSUserInterfaceItemIdentifier {
  static let testCell = NSUserInterfaceItemIdentifier.init("TestCell")
}
